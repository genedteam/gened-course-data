# GenEd Course Data #

This repo contains the GenEd course data including the full Spring 2017 course inventory, syllabi in .docx and .txt format, and a plaintext OpenNLP doccat model. 

Within the CourseSyllabi directory there is a node script to change .docx to .txt. The R folder is https://bitbucket.org/genedteam/r-clusters

MIT License