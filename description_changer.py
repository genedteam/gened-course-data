#Changes the description in the course_json.json file

import json
import codecs
import os

DESCRIPTION = "Students will read important works of world literature, philosophy, and religion, from ancient epics to graphic novels, with a focus on individual well-being. We will ask questions like: What do we value, and why? What makes for happiness? What's right and wrong? How is what's good for me defined by my relation to others? What is the purpose of life?"
COURSE_NAME = "Honors Intellectual Heritage I: The Good Life" #Put the course number here, matching that in the JSON
COURSE_NUM = "0851"

with open("course_json.json") as data_file:
	data = json.load(data_file)

name_found = False
num_found = False
found = False
for each in data:
	for key, value in each.items():
		if key == "num" and value == COURSE_NUM:
			num_found = True
		if key == "name" and value == COURSE_NAME:
			name_found = True


		if num_found and name_found:
			each["description"] = DESCRIPTION

			num_found = False
			name_found = False
			found = True

if not found:
	print("Course name + num not found.")

with open("course_json.json", "w+") as output:
	json.dump(data, output)
