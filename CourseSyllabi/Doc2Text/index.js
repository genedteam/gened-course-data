const fs = require('fs'),
      path = require('path'),
      textract = require('textract'),
      glob = require('glob'),
      docDir = path.resolve(__dirname, '../Docs/'),
      txtDir = path.resolve(__dirname, '../Text/'),
      deptNum = /[A-Z]{2,4} [0-9]{4}/m;

let name;

const retErr = (err) => console.log(err.toString());

glob(docDir + '/**/*.doc?', (err, files) => {

    if(err) return retErr(err);
    files.map(doc2txt);

    return;
});

function doc2txt(fileName){

    textract.fromFileWithPath(fileName, (err, txt) =>{
        if(err) return retErr(err);
        let courseCode;
        try{
            courseCode = (fileName.match(deptNum)[0]);
        }catch(e){
            return;
        }
        if(!courseCode) return;

        const file = path.resolve(txtDir, courseCode + '.txt');

        fs.open(file, 'w+', (err, fd) => {

            if(err){
                if(err.code === "EEXISTS"){
                    console.log(`${file} already exists`);
                }
                return retErr(err);
            }

            const wStream = fs.createWriteStream('', {fd: fd});
            wStream.write(txt + '\n', () => console.log(`Wrote ${file}`));
        });
    });

    return;
}
