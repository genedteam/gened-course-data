#Isaac Samuel's crappy description + syllabus combiner
#Parses JSON, finds a course's description, combines it with a course's Syllabus (From CourseSyllabi/Text) with and 
#then fixes the category in the source json file.

import json
import codecs
import os
import requests

with open("course_json.json") as data_file:
	data = json.load(data_file)


with open("json_output.json", "w+") as output:
	num = None #Must initialize or throws errors
	found = False
	for each in data:
		for key, value in each.items():
			if key == "num":
				num = value
			if key == "description":
				description = value
				description = description.replace("\n", "")
				description = description.replace("\\", "")
				each["description"] = description
			if key == "dept":
				departments = value

			if (num != None and description != None and departments != None and key == "categories"):
				found = True
				for dept in departments:
					for file_name in os.listdir('./CourseSyllabi/Text/'):
						file_name = file_name.split(".")[0]
						if file_name == (dept + " " + num):
							name_of_file = file_name + '.txt'
							file = open('./CourseSyllabi/Text/' + name_of_file)
							
							#Makes request to webservice (description + file.read().decode('utf-8'))
							payload = {"descriptions": [description + file.read().decode('utf-8')]}

							response = requests.post('http://localhost:8080', json = payload)
							temp = response.text.replace("_", " ")
							temp = temp.replace("\" ", "\"")
							temp = temp.replace("categories", "category")
							temp = json.loads(temp)


							if each["name"] != "Latino Immigration" and each["name"] != "Creativity and Organizational Innovation":
								each["categories"] = []
								for a in temp["results"]:
									a["category"] = a["category"][0]
									if a["score"] > .6:
										each["categories"].append(a)
							

							num = None
							description = None
							departments = None
							found = True
							break
					if found == True:
						found = False
						break

	json.dump(data, output)
'''
with open("json_output.json", "r") as output:
	#Cleans up output
	data = output.read().replace("\"", "'")
	data = data.replace("\\", "")

with open("json_output.json", "w") as output:
	output.write(data)
'''